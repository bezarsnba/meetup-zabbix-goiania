## Slides das palestrar do 1º Meetup Zabbix Goiânia que ocorreu no dia 9/11/2019.

Palestrantes:

Talk 1: Como Agregar valor ao cliente \
Nome: Fransuelo Timbo \
Linkedin: https://www.linkedin.com/in/fransuelo-timbo-85952238/ 

Talk 2: Meetup Zabbix - Threat Control com Vulners \
Nome: Vitor Luiz \
Linkedin: https://www.linkedin.com/in/vitor-luiz-10353633/ 

Talk 3: Monitorando banco de dados com ODBC \
Nome: Bezaleel Silva \
Linkedin: https://www.linkedin.com/in/bezarsnba 

Talk 4: SLA e Correta maneira de apuração \
Nome: Heitor Oliveira \
Linkedin: https://www.linkedin.com/in/heitoroliveira/ 

Talk 5: Zabbix + IOT - Inforamções na palma da mao \
Nome: Samuel Gonçalves \
Linkedin: https://www.linkedin.com/in/samuelgoncalvespereira/ 
